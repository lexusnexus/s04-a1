--a
SELECT * FROM artists WHERE name LIKE "%D%";
--b
SELECT * FROM songs WHERE length < 230;
--c
SELECT albums.album_name, songs.title, songs.length
FROM songs
INNER JOIN albums ON songs.album_id=albums.id;
--d
SELECT *
FROM albums
INNER JOIN artists ON albums.artist_id=artists.id
WHERE albums.album_name LIKE "%A%";
--e
SELECT * FROM albums
ORDER BY album_name DESC
LIMIT 4;
--f-1
SELECT *
FROM songs
INNER JOIN albums ON songs.album_id=albums.id
ORDER BY albums.album_name DESC;

--f-2
SELECT *
FROM songs
INNER JOIN albums ON songs.album_id=albums.id
ORDER BY songs.title;
